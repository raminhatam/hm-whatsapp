package ir.raminhm.hmwhatsapp.Models;

public class Users {

    String profilePic, userName, passWord, mail, userId, lastMessage, status;

    public Users(String profilePic, String userName, String passWord, String mail, String userId, String lastMessage, String status) {
        this.profilePic = profilePic;
        this.userName = userName;
        this.passWord = passWord;
        this.mail = mail;
        this.userId = userId;
        this.lastMessage = lastMessage;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public Users (){}

    // SignUp Constractor
    public Users (String userName, String passWord, String mail){
        this.userName = userName;
        this.passWord = passWord;
        this.mail = mail;
    }

    public String getProfilePic() {
        return profilePic;
    }

    public void setProfilePic(String profilePic) {
        this.profilePic = profilePic;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }


    public String getLastMessage() {
        return lastMessage;
    }

    public void setLastMessage(String lastMessage) {
        this.lastMessage = lastMessage;
    }
}
